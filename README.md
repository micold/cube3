# Scott Yeung - Cube 3 Agency FED Exercise

## Download and Installation

* Clone the repo: `git clone https://gitlab.com/micold/cube3.git`
* Run `npm install gulp`
* Start with `gulp dev` for any live reload changes.

#### Other Gulp Tasks

- `gulp` the default task that builds everything
- `gulp dev` browserSync opens the project in your default browser and live reloads when changes are made
- `gulp sass` compiles SCSS files into CSS
- `gulp minify-css` minifies the compiled CSS file
- `gulp minify-js` minifies the themes JS file
- `gulp copy` copies dependencies from node_modules to the vendor directory

## Bugs and Issues

Have a bug or an issue with this FED exercise? Please don't hesitate to mail to micold@gmail.com.


