(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 57)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 57
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);
  
  // Burger menu trigger fade-in animation for extra menu 
	$('a.target-burger').click(function(e){
    $('body').toggleClass('no-scroll'); // Fixed scrolling on body

  // Animate blur when the burger menu is clicked
    if($('.overlay').hasClass('blur-in cover')){ 
      $('.overlay-content').css("z-index",-1); //set overlay content on top with z-index.
      $('.overlay').toggleClass('blur-out'); 
      $('.overlay').toggleClass('blur-in cover'); 
    }
    else{
      $('.overlay-content').css("z-index",1); 
      $('.overlay').removeClass('blur-out'); 
      $('.overlay').toggleClass('blur-in cover'); 
    }

    $('a.target-burger').toggleClass('toggled');
    $('li.bun.first').toggleClass('hidden');
    
    if($('#mainNav').hasClass('navbar-shrink'))
      $('#mainNav').toggleClass('navbar-shrink');
    
    renewElement($(".overlay-content"));
    var $animated = $(".overlay-content");
    var shown = $animated.hasClass('overlay-show');
    $animated.toggleClass('overlay-show', !shown).toggleClass('overlay-hidden', shown);
  }); //target-burger-click

  // Clone .overlay-content and attach to the body again
  function renewElement(e) {
    var newElement = e.clone(true);
    e.remove();
    $("#page-top").append(newElement);
  }

  // Scroll reveal calls
  window.sr = ScrollReveal();
  sr.reveal('#carouselExampleIndicators', {
    duration: 600,
    scale: 0.3,
    distance: '0px'
  }, 200);
  sr.reveal('.sr-button', {
    duration: 1000,
    delay: 200
  });
  sr.reveal('#contact', {
    duration: 600,
    scale: 0.3,
    distance: '0px'
  }, 300);

  // Magnific popup calls
  $('.popup-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1]
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
    }
  });

})(jQuery); // End of use strict

$(window).scroll(function() {

  var offset = $(window).scrollTop();
  offset = offset * 0.002; //Set the scale with offset from $(window).scrollTop();

  if (($(window).scrollTop() * 0.002) < 1)
      $('#scroll-transform').css({
          '-webkit-transform': 'translate3d(0px, 0px, 0px) scale(' + offset + ',' + offset + ')'
      });
  else
      $('#scroll-transform').css({
          '-webkit-transform': 'matrix(1, 0, 0, 1, 0, 0)' // Stop increasing offset when it reaches to 1.
      });

  if (!$(window).scrollTop() > 0) {
      $('#scroll-transform').css({
          '-webkit-transform': 'matrix(0, 0, 0, 0, 0, 0)' // Set to initial state.
      });

      offset = 0;
  }
});